from __future__ import annotations
from typing import List, Tuple
import sys
import os
import xml.etree.ElementTree as gfg


class TokenTypeValidator:
    keywords = [
        'class', 'constructor', 'function',
        'method', 'field', 'static', 'var', 'int',
        'char', 'boolean', 'void', 'true', 'false', 'null',
        'this', 'that', 'let', 'do', 'if', 'else', 'while', 'return'
    ]

    symbols = [
        '{', '}', '(', ')', '[', ']', '.', ',', ';', '+', '-', '*', '/', '&', '|', '<', '>', '=', '~'
    ]

    def is_argument(self, token) -> bool:
        args = token.split(" ")
        if len(args) < 2:
            return False
        for arg in args:
            if '"' in arg:
                return False
        return True

    def _is_method(self, token) -> bool:
        if '(' in token and ')' in token:
            return self._is_identifier(token.split('(')[0])
        return False

    def _is_array(self, token) -> bool:
        return '[' in token and ']' in token

    def _is_identifier(self, token: str) -> bool:
        if '_' in token:
            token = token.replace("_", "")
        if not token.isalnum():
            return False

        return not self._is_int(token[0])

    def _is_string(self, token: str) -> bool:
        return token.startswith('"') or token.startswith("'")

    def _is_keyword(self, token: str) -> bool:
        return token in self.keywords

    def _is_symbol(self, token: str) -> bool:
        return token in self.symbols

    def _is_int(self, token) -> bool:
        try:
            int(token)
        except ValueError:
            return False
        else:
            return True


class Tokenizer:
    validator = TokenTypeValidator()

    def define_types(self, token: str) -> List[Tuple[str, str]]:
        types = []

        print(f"Defining type for {token=}")

        if len(token) > 2:
            if '.' in token:
                types += self._dissect_object_type(token)
                return types
            elif self.validator._is_method(token):
                types += self._define_method_type(token)
                return types
            elif self.validator._is_array(token):
                types += self._define_array_type(token)
                return types
            elif self.validator.is_argument(token):
                types += self._dissect_argument_type(token)
                return types

        first_els, token = self._dissect_first_el(token)
        last_els, token = self._dissect_last_el(token)

        if len(token) == 0:
            return first_els + last_els

        if self.validator._is_symbol(token):
            types.append((token, 'symbol'))
        elif self.validator._is_int(token):
            types.append((token, 'integerConstant'))
        elif self.validator._is_string(token):
            types.append((token.replace('"', ""), 'stringConstant'))
        elif self.validator._is_keyword(token):
            types.append((token, 'keyword'))
        elif self.validator._is_identifier(token):
            types.append((token, 'identifier'))
        else:
            raise ValueError(f"Provided value can not be distinguished: {token=}!")

        types = first_els + types + last_els
        return types

    def _dissect_first_el(self, token: str) -> Tuple[List[Tuple[str, str]], str]:
        first_els = []
        for i in range(len(token)):
            try:
                first_token_el = token[i]
            except IndexError:
                break
            else:
                if not self.validator._is_symbol(first_token_el):
                    break
                first_els.append((first_token_el, 'symbol'))
        token = token[len(first_els):]
        return first_els, token

    def _dissect_last_el(self, token: str) -> Tuple[List[Tuple[str, str]], str]:
        last_els = []
        for i in range(len(token) - 1, 0, -1):
            try:
                last_token_el = token[i]
            except IndexError:
                break
            else:
                if not self.validator._is_symbol(last_token_el):
                    break
                last_els.insert(0, (last_token_el, 'symbol'))
                token = token[:-1]
        return last_els, token

    def _dissect_object_type(self, token: str) -> List[Tuple[str, str]]:
        types = []

        args = token.split('.')
        args.insert(1, '.')
        for arg in args:
            if arg == '':
                continue
            types += self.define_types(arg)

        return types

    def _dissect_argument_type(self, token: str) -> List[Tuple[str, str]]:
        types = []

        args = token.split(' ')
        for arg in args:
            if arg == '':
                continue
            types += self.define_types(arg)

        return types

    def _define_method_type(self, token: str) -> List[Tuple[str, str]]:
        types = []

        args = token.split('(', 1)
        args.insert(1, "(")
        func_args = args[2].split(", ")
        len_func_args = len(func_args)
        inserted = 1
        for i in range(len_func_args):
            if i + 1 >= len_func_args:
                break
            func_args.insert(i + 1 * inserted, ',')
            inserted += 1

        args = args[:2] + func_args[:]
        for arg in args:
            if arg == '':
                continue
            types += self.define_types(arg)

        return types

    def _define_array_type(self, token: str) -> List[Tuple[str, str]]:
        types = []
        args = token.split('[')
        args.insert(1, "[")
        for arg in args:
            if arg == '':
                continue
            types += self.define_types(arg)

        return types


class Grammizer:
    def __init__(self, tokens, xml_builder: XMLBuilder):
        self.xml_builder = xml_builder
        self.tokens = tokens
        self.statements = None

    def dissect_grammar(self) -> XMLBuilder:
        cursor = 0
        for _ in range(len(self.tokens)):
            try:
                text, type = self.tokens[cursor]
            except IndexError:
                break

            if text in ['static', 'field']:
                cursor += self._parse_class_var(cursor)
            elif text in ["constructor", "function", "method"]:
                cursor += self._parse_class_subroutine(cursor)
            else:
                cursor += 1
                self.xml_builder.append_token(self.xml_builder.root, type, text)
        return self.xml_builder

    def _parse_class_var(self, i) -> int:
        parent = self.xml_builder.append_element(self.xml_builder.root, "classVarDec")
        tokens = self.tokens[i:]
        for j in range(len(tokens)):
            text, type = tokens[j]
            self.xml_builder.append_token(parent, type, text)
            if text == ';':
                break

        return j + 1

    def _parse_class_subroutine(self, i) -> int:
        parent = self.xml_builder.append_element(self.xml_builder.root, "subroutineDec")
        tokens = self.tokens[i:]
        cursor = 0
        for _ in range(len(tokens)):
            text, type = tokens[cursor]
            if text == '{':
                cursor += self._parse_sub_body(parent, i + cursor)
                break
            self.xml_builder.append_token(parent, type, text)
            cursor += 1
            if text == '(':
                cursor += self._parse_parameter_list(parent, i + cursor)

        return cursor

    def _parse_parameter_list(self, parent, i) -> int:
        parent = self.xml_builder.append_element(parent, "parameterList")
        tokens = self.tokens[i:]
        cursor = 0
        for _ in range(len(tokens)):
            text, type = tokens[cursor]
            if text == ')':
                break

            self.xml_builder.append_token(parent, type, text)
            cursor += 1

        return cursor

    def _parse_sub_body(self, parent, i) -> int:
        parent = self.xml_builder.append_element(parent, "subroutineBody")

        tokens = self.tokens[i:]
        cursor = 0
        for _ in range(len(tokens)):
            text, type = tokens[cursor]
            if text == '{':
                cursor += 1
                self.xml_builder.append_token(parent, type, text)
            elif text == '}':
                cursor += 1
                self.xml_builder.append_token(parent, type, text)
                break
            elif text == 'var':
                cursor += self._parse_var_dec(parent, i + cursor)
                continue
            elif text in ['let', 'if', 'while', 'do', 'return']:
                cursor += self._parse_statements(parent, i + cursor)
            else:
                raise ValueError(f"Provided token is not supported: {text=} and {type=} !")

        return cursor

    def _parse_var_dec(self, parent, i) -> int:
        parent = self.xml_builder.append_element(parent, "varDec")
        tokens = self.tokens[i:]

        for j in range(len(tokens)):
            text, type = tokens[j]
            self.xml_builder.append_token(parent, type, text)
            if text == ';':
                break

        return j + 1

    def _parse_statements(self, parent, i) -> int:
        parent = self.xml_builder.append_element(parent, "statements")
        tokens = self.tokens[i:]
        cursor = 0
        for _ in tokens:
            text, type = tokens[cursor]
            if text in ['let', 'if', 'while', 'do', 'return']:
                cursor += self._parse_statement(parent, i + cursor)
            else:
                break

        return cursor

    def _parse_statement(self, parent, i) -> int:
        text, type = self.tokens[i]
        method = "_parse_%s_stat" % text
        return getattr(self, method)(parent, i)

    def _parse_let_stat(self, parent, i) -> int:
        parent = self.xml_builder.append_element(parent, "letStatement")
        tokens = self.tokens[i:]

        cursor = 0
        next_is_expression = False
        for _ in range(len(tokens)):
            if next_is_expression:
                cursor += self._parse_expression(parent, i + cursor)
                next_is_expression = False
                continue
            text, type = tokens[cursor]
            self.xml_builder.append_token(parent, type, text)
            cursor += 1
            if text in ['[', '=']:
                next_is_expression = True
            elif text == ';':
                break
        return cursor

    def _parse_if_stat(self, parent, i) -> int:
        parent = self.xml_builder.append_element(parent, "ifStatement")
        tokens = self.tokens[i:]
        cursor = 0

        next_is_expression = False
        next_is_statements = False
        for _ in range(len(tokens)):
            if next_is_expression:
                cursor += self._parse_expression(parent, i + cursor)
                next_is_expression = False
                continue
            elif next_is_statements:
                cursor += self._parse_statements(parent, i + cursor)
                next_is_statements = False
                continue

            text, type = tokens[cursor]
            self.xml_builder.append_token(parent, type, text)
            cursor += 1

            if text == '(':
                next_is_expression = True
            elif text == '{':
                next_is_statements = True
            elif text == '}':
                text, type = tokens[cursor]
                if text == 'else':
                    continue
                else:
                    break

        return cursor

    def _parse_while_stat(self, parent, i) -> int:
        parent = self.xml_builder.append_element(parent, "whileStatement")
        tokens = self.tokens[i:]
        cursor = 0

        next_is_expression = False
        next_is_statements = False
        for _ in range(len(tokens)):
            if next_is_expression:
                cursor += self._parse_expression(parent, i + cursor)
                next_is_expression = False
                continue
            elif next_is_statements:
                cursor += self._parse_statements(parent, i + cursor)
                next_is_statements = False
                continue

            text, type = tokens[cursor]
            self.xml_builder.append_token(parent, type, text)
            cursor += 1

            if text == '(':
                next_is_expression = True
            elif text == '{':
                next_is_statements = True
            elif text == '}':
                break

        return cursor

    def _parse_do_stat(self, parent, i) -> int:
        parent = self.xml_builder.append_element(parent, "doStatement")
        cursor = 0

        text, type = self.tokens[i]
        self.xml_builder.append_token(parent, type, text)
        cursor += 1

        cursor += self._parse_subroutine_call(parent, i + cursor)

        text, type = self.tokens[i + cursor]
        self.xml_builder.append_token(parent, type, text)
        cursor += 1

        return cursor

    def _parse_return_stat(self, parent, i) -> int:
        parent = self.xml_builder.append_element(parent, "returnStatement")
        cursor = 0
        next_expression = False
        tokens = self.tokens[i:]
        for _ in tokens:
            text, type = tokens[cursor]
            if text == ';':
                self.xml_builder.append_token(parent, type, text)
                cursor += 1
                break
            if next_expression:
                cursor += self._parse_expression(parent, i + cursor)
                next_expression = False
                continue
            self.xml_builder.append_token(parent, type, text)
            next_expression = True
            cursor += 1

        return cursor

    def _parse_expression(self, parent, i) -> int:
        op = ['+', '-', '*', '/', '&', '|', '<', '>', '=']
        was_previous_op = False
        expression = self.xml_builder.append_element(parent, "expression")
        tokens = self.tokens[i:]
        cursor = 0
        for _ in range(len(tokens)):
            text, type = tokens[cursor]
            if was_previous_op or cursor == 0:
                was_previous_op = False
                term_parent = self.xml_builder.append_element(expression, "term")
                cursor += self._parse_term(term_parent, i + cursor)
            else:
                if text in op:
                    self.xml_builder.append_token(expression, type, text)
                    cursor += 1
                    was_previous_op = True
                else:
                    break
        return cursor

    def _parse_term(self, parent, i) -> int:
        unary_op = ['-', '~']
        cursor = 0
        text, type = self.tokens[i]

        if type in ['keyword', 'integerConstant', 'stringConstant']:
            self.xml_builder.append_token(parent, type, text)
            cursor += 1
            return cursor
        elif text in unary_op:
            self.xml_builder.append_token(parent, type, text)
            cursor += 1
            term_parent = self.xml_builder.append_element(parent, "term")
            cursor += self._parse_term(term_parent, cursor + i)
            return cursor
        elif type == 'identifier':
            self.xml_builder.append_token(parent, type, text)
            cursor += 1
            text, type = self.tokens[i + cursor]
            if text == '[':
                self.xml_builder.append_token(parent, type, text)
                cursor += 1

                cursor += self._parse_expression(parent, i + cursor)
                self.xml_builder.append_token(parent, type, ']')
                cursor += 1
            elif text == '(' or text == '.':
                cursor += self._parse_subroutine_call(parent, i + cursor)

            return cursor
        elif text == '(':
            self.xml_builder.append_token(parent, type, text)
            cursor = self._parse_expression(parent, i + 1)
            self.xml_builder.append_token(parent, type, ')')
            cursor += 2
            return cursor
        else:
            raise ValueError(f"Provided token {text=} an {type=} are not supported")

    def _parse_subroutine_call(self, parent, i) -> int:
        tokens = self.tokens[i:]
        cursor = 0
        for _ in range(len(tokens)):
            text, type = tokens[cursor]
            self.xml_builder.append_token(parent, type, text)
            cursor += 1
            if text == ')':
                break
            if text == '(':
                cursor += self._parse_expression_list(parent, cursor + i)

        return cursor

    def _parse_expression_list(self, parent, i) -> int:
        tokens = self.tokens[i:]
        cursor = 0

        parent = self.xml_builder.append_element(parent, "expressionList")
        was_previous_expression = False
        for _ in range(len(tokens)):
            text, type = tokens[cursor]
            if text == ')':
                break
            if was_previous_expression:

                if text == ',':
                    was_previous_expression = False
                    self.xml_builder.append_token(parent, type, text)
                    cursor += 1
                    continue
                break
            else:
                was_previous_expression = True
                try:
                    cursor += self._parse_expression(parent, i + cursor)
                except ValueError:
                    break

        return cursor


class XMLBuilder:
    root: gfg.Element

    def __init__(self):
        self.root = gfg.Element("class")

    def append_element(self, parent: gfg.Element, name: str):
        el = gfg.Element(name)
        parent.append(el)
        return el

    def append_token(self, parent, element, text):
        child = gfg.SubElement(parent, element)
        child.text = text

    def generate_tree(self):
        return gfg.ElementTree(self.root)


def compile_file(lines: List[str], xml_builder: XMLBuilder) -> XMLBuilder:
    tokens = Tokenizer()

    tokens_types = []
    comments = False
    for line in lines:
        line = line.strip()
        if ("/**" in line and '*/' in line) or line == '' or line.startswith('//') or comments:
            continue
        if '/**' in line:
            comments = True
            continue
        if '*/' in line:
            comments = False
            continue
        if '//' in line:
            comment = line.find('//')
            line = line[:comment].strip()

        print(f"Compiling line: {line}")
        for token in _extract_tokens(line):
            tokens_types += tokens.define_types(token)

    gram = Grammizer(tokens_types, xml_builder)
    return gram.dissect_grammar()


def _extract_tokens(line: str) -> List[str]:
    tokens = []
    while len(line) > 0:
        new_token = _extract_token(line)
        tokens.append(new_token)
        new_start = line.find(new_token) + len(new_token) + 1
        line = line[new_start:]
    return tokens


def _extract_token(line: str) -> str:
    token = ''
    open_quotation = False
    open_bracket = 0
    for el in line:
        if el in ["'", '"']:
            open_quotation = not open_quotation
        if el == '(':
            open_bracket += 1
        if el == ')':
            open_bracket -= 1

        if el == ' ':
            if open_quotation or open_bracket > 0:
                token += el
                continue
            if len(token) == 0:
                continue
            break

        token += el

    return token


def main(filename: str) -> None:
    if os.path.isfile(filename):
        xml_builder = XMLBuilder()
        print(f"Starting to compile file: {filename}")
        xml_builder = handle_file(filename, xml_builder)
        tree = xml_builder.generate_tree()

        output_file = filename.replace('jack', 'xml')
        with open("opt/" + output_file, 'wb') as f:
            tree.write(f, encoding="utf-8", xml_declaration=False, short_empty_elements=False)

        print('Done')
        return

    for file in os.listdir(filename):
        if file.endswith(".jack"):
            xml_builder = XMLBuilder()
            print(f"Starting to compile file: {filename + file}")
            xml_builder = handle_file(filename + file, xml_builder)
            tree = xml_builder.generate_tree()
            output_file = file.replace('jack', 'xml')
            with open(filename + "opt/" + output_file, 'wb') as f:
                gfg.indent(tree)
                tree.write(f, encoding="utf-8", xml_declaration=False, short_empty_elements=False)

    print('Done')


def handle_file(filename: str, xml_builder: XMLBuilder) -> XMLBuilder:
    source_code = open(filename, 'r')
    lines = source_code.readlines()
    tokens = compile_file(lines, xml_builder)
    source_code.close()

    return tokens


if __name__ == '__main__':
    main(sys.argv[1])
