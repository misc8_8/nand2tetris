// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

@8192
D=A
@upper_limit
M=D

@current
M=0

@LOOP

(LOOP)
    @KBD
    D=M
    @PAINT_WHITE
    D;JEQ
    @PAINT_BLACK
    D;JNE

(PAINT_WHITE)
    @current
    D=M
    @LOOP
    D;JLT

    @SCREEN
    A=A+D
    M=0
    @current
    M=M-1
    @LOOP
    0;JMP

(PAINT_BLACK)
    @upper_limit
    D=M
    @current
    D=M-D
    @LOOP
    D;JGE

    @current
    D=M
    @SCREEN
    A=A+D
    M=-1
    @current
    M=M+1
    @LOOP
    0;JMP






