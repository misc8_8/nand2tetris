// push constant 17
@17
D=A
@SP
A=M
M=D
@SP
M=M+1

// push constant 17
@17
D=A
@SP
A=M
M=D
@SP
M=M+1

// eq
@SP
AM=M-1
D=M
@SP
AM=M-1
D=M-D
@EQUALS_ZERO_1
D;JEQ
@SP
A=M
M=0
@EQ_ZERO_MOVE_POINTER_1
0;JMP
(EQUALS_ZERO_1)
@SP
A=M
M=-1
(EQ_ZERO_MOVE_POINTER_1)
@SP
M=M+1

// push constant 17
@17
D=A
@SP
A=M
M=D
@SP
M=M+1

// push constant 16
@16
D=A
@SP
A=M
M=D
@SP
M=M+1

// eq
@SP
AM=M-1
D=M
@SP
AM=M-1
D=M-D
@EQUALS_ZERO_2
D;JEQ
@SP
A=M
M=0
@EQ_ZERO_MOVE_POINTER_2
0;JMP
(EQUALS_ZERO_2)
@SP
A=M
M=-1
(EQ_ZERO_MOVE_POINTER_2)
@SP
M=M+1

// push constant 16
@16
D=A
@SP
A=M
M=D
@SP
M=M+1

// push constant 17
@17
D=A
@SP
A=M
M=D
@SP
M=M+1

// eq
@SP
AM=M-1
D=M
@SP
AM=M-1
D=M-D
@EQUALS_ZERO_3
D;JEQ
@SP
A=M
M=0
@EQ_ZERO_MOVE_POINTER_3
0;JMP
(EQUALS_ZERO_3)
@SP
A=M
M=-1
(EQ_ZERO_MOVE_POINTER_3)
@SP
M=M+1

// push constant 892
@892
D=A
@SP
A=M
M=D
@SP
M=M+1

// push constant 891
@891
D=A
@SP
A=M
M=D
@SP
M=M+1

// lt
@SP
AM=M-1
D=M
@SP
AM=M-1
D=M-D
@LT_ZERO_4
D;JLT
@SP
A=M
M=0
@LT_MOVE_POINTER_4
0;JMP
(LT_ZERO_4)
@SP
A=M
M=-1
(LT_MOVE_POINTER_4)
@SP
M=M+1

// push constant 891
@891
D=A
@SP
A=M
M=D
@SP
M=M+1

// push constant 892
@892
D=A
@SP
A=M
M=D
@SP
M=M+1

// lt
@SP
AM=M-1
D=M
@SP
AM=M-1
D=M-D
@LT_ZERO_5
D;JLT
@SP
A=M
M=0
@LT_MOVE_POINTER_5
0;JMP
(LT_ZERO_5)
@SP
A=M
M=-1
(LT_MOVE_POINTER_5)
@SP
M=M+1

// push constant 891
@891
D=A
@SP
A=M
M=D
@SP
M=M+1

// push constant 891
@891
D=A
@SP
A=M
M=D
@SP
M=M+1

// lt
@SP
AM=M-1
D=M
@SP
AM=M-1
D=M-D
@LT_ZERO_6
D;JLT
@SP
A=M
M=0
@LT_MOVE_POINTER_6
0;JMP
(LT_ZERO_6)
@SP
A=M
M=-1
(LT_MOVE_POINTER_6)
@SP
M=M+1

// push constant 32767
@32767
D=A
@SP
A=M
M=D
@SP
M=M+1

// push constant 32766
@32766
D=A
@SP
A=M
M=D
@SP
M=M+1

// gt
@SP
AM=M-1
D=M
@SP
AM=M-1
D=M-D
@GT_ZERO_7
D;JGT
@SP
A=M
M=0
@GT_MOVE_POINTER_7
0;JMP
(GT_ZERO_7)
@SP
A=M
M=-1
(GT_MOVE_POINTER_7)
@SP
M=M+1

// push constant 32766
@32766
D=A
@SP
A=M
M=D
@SP
M=M+1

// push constant 32767
@32767
D=A
@SP
A=M
M=D
@SP
M=M+1

// gt
@SP
AM=M-1
D=M
@SP
AM=M-1
D=M-D
@GT_ZERO_8
D;JGT
@SP
A=M
M=0
@GT_MOVE_POINTER_8
0;JMP
(GT_ZERO_8)
@SP
A=M
M=-1
(GT_MOVE_POINTER_8)
@SP
M=M+1

// push constant 32766
@32766
D=A
@SP
A=M
M=D
@SP
M=M+1

// push constant 32766
@32766
D=A
@SP
A=M
M=D
@SP
M=M+1

// gt
@SP
AM=M-1
D=M
@SP
AM=M-1
D=M-D
@GT_ZERO_9
D;JGT
@SP
A=M
M=0
@GT_MOVE_POINTER_9
0;JMP
(GT_ZERO_9)
@SP
A=M
M=-1
(GT_MOVE_POINTER_9)
@SP
M=M+1

// push constant 57
@57
D=A
@SP
A=M
M=D
@SP
M=M+1

// push constant 31
@31
D=A
@SP
A=M
M=D
@SP
M=M+1

// push constant 53
@53
D=A
@SP
A=M
M=D
@SP
M=M+1

// add
@SP
AM=M-1
D=M
@SP
AM=M-1
M=D+M
@SP
M=M+1

// push constant 112
@112
D=A
@SP
A=M
M=D
@SP
M=M+1

// sub
@SP
AM=M-1
D=M
@SP
AM=M-1
M=M-D
@SP
M=M+1

// neg
@SP
AM=M-1
M=-M
@SP
M=M+1

// and
@SP
AM=M-1
D=M
@SP
AM=M-1
M=D&M
@SP
M=M+1

// push constant 82
@82
D=A
@SP
A=M
M=D
@SP
M=M+1

// or
@SP
AM=M-1
D=M
@SP
AM=M-1
M=D|M
@SP
M=M+1

// not
@SP
AM=M-1
M=!M
@SP
M=M+1

