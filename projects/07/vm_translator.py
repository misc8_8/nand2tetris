from typing import List
import sys

ram = {
    "sp": "SP",
    "local": "LCL",
    "argument": "ARG",
    "this": "THIS",
    "that": "THAT",
    "temp": 5,
}

def to_assembly(instructions: List[str], filename: str) -> List[str]:
    assembly_code = []
    commands = Commands()
    stack_man = StackManipulation()

    for instr in instructions:
        instr = instr.strip()
        if instr.startswith("//") or instr == '':
            continue

        print(f"starting to compile the instruction: {instr}")
        assembly_code.append("// "+instr)
        ingr = instr.split(" ")
        if len(ingr) == 3:
            code = stack_man.to_assembly(int(ingr[2]), ingr[1], filename, ingr[0])
        elif len(ingr) == 1:
            code = commands.to_assembly(ingr[0])
        else:
            raise ValueError(f"Provided instruction is not correct: {instr}!")
        assembly_code += code
        assembly_code.append("")

    return assembly_code

class Commands:
    i = 0
    
    def to_assembly(self, command: str) -> List[str]:
        cmd = '%s_to_assembly' % command
        self.i += 1
        return getattr(self, cmd)(self.i)


    def add_to_assembly(self, i: int) -> List[str]:
        assembly_code = []


        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("M=D+M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code


    def sub_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("M=M-D")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code
    
    def neg_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("M=-M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code
    
    def eq_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M-D")
        assembly_code.append(f"@EQUALS_ZERO_{i}")
        assembly_code.append("D;JEQ")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=0")
        assembly_code.append(f"@EQ_ZERO_MOVE_POINTER_{i}")
        assembly_code.append("0;JMP")

        assembly_code.append(f"(EQUALS_ZERO_{i})")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=-1")

        assembly_code.append(f"(EQ_ZERO_MOVE_POINTER_{i})")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code
    
    def gt_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M-D")
        assembly_code.append(f"@GT_ZERO_{i}")
        assembly_code.append("D;JGT")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=0")
        assembly_code.append(f"@GT_MOVE_POINTER_{i}")
        assembly_code.append("0;JMP")

        assembly_code.append(f"(GT_ZERO_{i})")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=-1")

        assembly_code.append(f"(GT_MOVE_POINTER_{i})")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code
    
    def lt_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M-D")
        assembly_code.append(f"@LT_ZERO_{i}")
        assembly_code.append("D;JLT")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=0")
        assembly_code.append(f"@LT_MOVE_POINTER_{i}")
        assembly_code.append("0;JMP")

        assembly_code.append(f"(LT_ZERO_{i})")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=-1")

        assembly_code.append(f"(LT_MOVE_POINTER_{i})")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code
    
    def and_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("M=D&M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code
    
    def or_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("M=D|M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code
    
    def not_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("M=!M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code

class StackManipulation:
    operations = {
        'local': '_%s',
        'argument': '_%s',
        'that': '_%s',
        'this': '_%s',
        'constant': '_%s_constant',
        'static': '_%s_static',
        'temp': '_%s_temp',
        'pointer': '_%s_pointer',

    }

    def to_assembly(self, i: int, memory_segment: str, filename: str, operation: str) -> List[str]:
        try:
            op = self.operations[memory_segment] % operation
        except KeyError as e:
            print(f'Provided {memory_segment=} is not supported by compiler {operation} instruction.')
            raise e from None
        else:
            return getattr(self, op)(i, memory_segment, filename)

    def _pop(self, i: int, memory_segment: str, filename: str) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{i}")
        assembly_code.append("D=A")
        assembly_code.append(f"@{ram[memory_segment]}")
        assembly_code.append("A=M+D")
        assembly_code.append("D=A")

        assembly_code.append(f"@{ram[memory_segment]}")
        assembly_code.append("M=D")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M-1")
        assembly_code.append("A=M")
        assembly_code.append("D=M")

        assembly_code.append(f"@{ram[memory_segment]}")
        assembly_code.append("A=M")
        assembly_code.append("M=D")

        assembly_code.append(f"@{i}")
        assembly_code.append("D=A")
        assembly_code.append(f"@{ram[memory_segment]}")
        assembly_code.append("D=M-D")
        assembly_code.append("M=D")

        return assembly_code

    def _push(self, i: int, memory_segment: str, filename: str) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{i}")
        assembly_code.append("D=A")
        assembly_code.append(f"@{ram[memory_segment]}")
        assembly_code.append("A=M+D")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")
        assembly_code.append("A=M-1")
        assembly_code.append("M=D")


        return assembly_code

    def _pop_constant(self, i: int, memory_segment: str, filename: str) -> List[str]:
        raise ValueError("POP constant is not supported!")

    def _push_constant(self, i: int, memory_segment: str, filename: str) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{i}")
        assembly_code.append("D=A")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")
        assembly_code.append("A=M-1")
        assembly_code.append("M=D")

        return assembly_code

    def _pop_static(self, i: int, memory_segment: str, filename: str) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")
        assembly_code.append(f"@{filename}.{i}")
        assembly_code.append("M=D")

        return assembly_code

    def _push_static(self, i: int, memory_segment: str, filename: str) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{filename}.{i}")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")
        assembly_code.append("A=M-1")
        assembly_code.append("M=D")

        return assembly_code

    def _pop_temp(self, i: int, memory_segment: str, filename: str) -> List[str]:
        assembly_code = []

        mem = ram['temp'] + i
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")
        assembly_code.append(f"@{mem}")
        assembly_code.append("M=D")

        return assembly_code

    def _push_temp(self, i: int, memory_segment: str, filename: str) -> List[str]:
        assembly_code = []

        mem = ram['temp'] + i
        assembly_code.append(f"@{mem}")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")
        assembly_code.append("A=M-1")
        assembly_code.append("M=D")

        return assembly_code

    def _pop_pointer(self, i: int, memory_segment: str, filename: str) -> List[str]:
        mem = ["this", "that"]

        assembly_code = []

        mem = ram[mem[i]]
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")
        assembly_code.append(f"@{mem}")
        assembly_code.append("M=D")

        return assembly_code

    def _push_pointer(self, i: int, memory_segment: str, filename: str) -> List[str]:
        mem = ["this", "that"]

        assembly_code = []

        mem = ram[mem[i]]
        assembly_code.append(f"@{mem}")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")
        assembly_code.append("A=M-1")
        assembly_code.append("M=D")

        return assembly_code

def main(filename: str) -> None:
    source_code = open(filename, 'r')
    lines = source_code.readlines()
    assembly_lines = to_assembly(lines, filename.split(".")[0])
    source_code.close()

    output_file = filename.replace('vm', 'asm')
    with open(output_file, 'w') as f:
        for line in assembly_lines:
            f.write(f"{line}\n")

    print('Done')

if __name__ == '__main__':
    main(sys.argv[1])