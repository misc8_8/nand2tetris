from typing import List
import sys
import os

ram = {
    "sp": "SP",
    "local": "LCL",
    "argument": "ARG",
    "this": "THIS",
    "that": "THAT",
    "temp": 5,
}
class Functions:
    n_calls = 1
    operations = {
        'function': '_%s_to_assembly',
        'call': '_%s_to_assembly',
        'return': '_%s_to_assembly',
        'sys_init': '_%s_to_assembly',
    }

    def to_assembly(self, operation: str, label_name: str, n_vars: int) -> List[str]:
        try:
            op = self.operations[operation] % operation
        except KeyError as e:
            print(f'Provided {operation=} is not supported by compiler instruction.')
            raise e from None
        else:
            return getattr(self, op)(label_name, n_vars)

    def _sys_init_to_assembly(self, label_name: str, n_vars: int) -> List[str]:
        assembly_code = []

        assembly_code.append("@256")
        assembly_code.append("D=A")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=D")

        assembly_code += self._call_to_assembly(label_name, n_vars)

        return assembly_code

    def _function_to_assembly(self, label_name: str, n_vars: int) -> List[str]:
        stack = StackManipulation()
        assembly_code = []
        assembly_code.append(f"({label_name})")

        for i in range(n_vars):
            assembly_code += stack.to_assembly(0, 'constant', '', 'push')

        return assembly_code

    def _call_to_assembly(self, label_name: str, n_vars: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{label_name}$return.{self.n_calls}")
        assembly_code.append("D=A")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=D")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        assembly_code.append(f"@{ram['local']}")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=D")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        assembly_code.append(f"@{ram['argument']}")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=D")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        assembly_code.append(f"@{ram['this']}")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=D")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        assembly_code.append(f"@{ram['that']}")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=D")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        assembly_code.append("D=M")
        assembly_code.append("@5")
        assembly_code.append("D=D-A")
        assembly_code.append(f"@{n_vars}")
        assembly_code.append("D=D-A")
        assembly_code.append(f"@{ram['argument']}")
        assembly_code.append("M=D")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['local']}")
        assembly_code.append("M=D") # LCL = SP

        assembly_code.append(f"@{label_name}")
        assembly_code.append("0;JMP")
        assembly_code.append(f"({label_name}$return.{self.n_calls})")

        self.n_calls += 1

        return assembly_code


class Commands:
    i = 0

    def to_assembly(self, command: str) -> List[str]:
        cmd = '%s_to_assembly' % command
        self.i += 1
        return getattr(self, cmd)(self.i)

    def return_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['local']}")
        assembly_code.append("D=M")
        assembly_code.append("@endFrame")
        assembly_code.append("M=D")  # endFrame = LCL

        assembly_code.append("@5")
        assembly_code.append("D=D-A")
        assembly_code.append("A=D")
        assembly_code.append("D=M")
        assembly_code.append("@returnAddress")
        assembly_code.append("M=D") #returnAddress = *(FRAME-5)

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['argument']}")
        assembly_code.append("A=M")
        assembly_code.append("M=D")

        assembly_code.append(f"@{ram['argument']}")
        assembly_code.append("D=M+1")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=D") #SP = ARG+1

        assembly_code.append("@endFrame")
        assembly_code.append("A=M-1")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['that']}")
        assembly_code.append("M=D") #restores THAT

        assembly_code.append("@2")
        assembly_code.append("D=A")
        assembly_code.append("@endFrame")
        assembly_code.append("A=M-D")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['this']}")
        assembly_code.append("M=D") #restores THIS

        assembly_code.append("@3")
        assembly_code.append("D=A")
        assembly_code.append("@endFrame")
        assembly_code.append("A=M-D")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['argument']}")
        assembly_code.append("M=D") #restores ARG

        assembly_code.append("@4")
        assembly_code.append("D=A")
        assembly_code.append("@endFrame")
        assembly_code.append("A=M-D")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['local']}")
        assembly_code.append("M=D") #restores LCL

        assembly_code.append("@returnAddress")
        assembly_code.append("A=M")
        assembly_code.append("0;JMP")

        return assembly_code

    def add_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("M=D+M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code

    def sub_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("M=M-D")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code

    def neg_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("M=-M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code

    def eq_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M-D")
        assembly_code.append(f"@EQUALS_ZERO_{i}")
        assembly_code.append("D;JEQ")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=0")
        assembly_code.append(f"@EQ_ZERO_MOVE_POINTER_{i}")
        assembly_code.append("0;JMP")

        assembly_code.append(f"(EQUALS_ZERO_{i})")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=-1")

        assembly_code.append(f"(EQ_ZERO_MOVE_POINTER_{i})")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code

    def gt_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M-D")
        assembly_code.append(f"@GT_ZERO_{i}")
        assembly_code.append("D;JGT")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=0")
        assembly_code.append(f"@GT_MOVE_POINTER_{i}")
        assembly_code.append("0;JMP")

        assembly_code.append(f"(GT_ZERO_{i})")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=-1")

        assembly_code.append(f"(GT_MOVE_POINTER_{i})")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code

    def lt_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M-D")
        assembly_code.append(f"@LT_ZERO_{i}")
        assembly_code.append("D;JLT")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=0")
        assembly_code.append(f"@LT_MOVE_POINTER_{i}")
        assembly_code.append("0;JMP")

        assembly_code.append(f"(LT_ZERO_{i})")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("A=M")
        assembly_code.append("M=-1")

        assembly_code.append(f"(LT_MOVE_POINTER_{i})")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code

    def and_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("M=D&M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code

    def or_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("M=D|M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code

    def not_to_assembly(self, i: int) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("M=!M")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")

        return assembly_code


class StackManipulation:
    operations = {
        'local': '_%s',
        'argument': '_%s',
        'that': '_%s',
        'this': '_%s',
        'constant': '_%s_constant',
        'static': '_%s_static',
        'temp': '_%s_temp',
        'pointer': '_%s_pointer',

    }

    def to_assembly(self, i: int, memory_segment: str, filename: str, operation: str) -> List[str]:
        try:
            op = self.operations[memory_segment] % operation
        except KeyError as e:
            print(f'Provided {memory_segment=} is not supported by compiler {operation} instruction.')
            raise e from None
        else:
            return getattr(self, op)(i, memory_segment, filename)

    def _pop(self, i: int, memory_segment: str, filename: str) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{i}")
        assembly_code.append("D=A")
        assembly_code.append(f"@{ram[memory_segment]}")
        assembly_code.append("A=M+D")
        assembly_code.append("D=A")

        assembly_code.append(f"@{ram[memory_segment]}")
        assembly_code.append("M=D")

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")

        assembly_code.append(f"@{ram[memory_segment]}")
        assembly_code.append("A=M")
        assembly_code.append("M=D")

        assembly_code.append(f"@{i}")
        assembly_code.append("D=A")
        assembly_code.append(f"@{ram[memory_segment]}")
        assembly_code.append("D=M-D")
        assembly_code.append("M=D")

        return assembly_code

    def _push(self, i: int, memory_segment: str, filename: str) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{i}")
        assembly_code.append("D=A")
        assembly_code.append(f"@{ram[memory_segment]}")
        assembly_code.append("A=M+D")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")
        assembly_code.append("A=M-1")
        assembly_code.append("M=D")

        return assembly_code

    def _pop_constant(self, i: int, memory_segment: str, filename: str) -> List[str]:
        raise ValueError("POP constant is not supported!")

    def _push_constant(self, i: int, memory_segment: str, filename: str) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{i}")
        assembly_code.append("D=A")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")
        assembly_code.append("A=M-1")
        assembly_code.append("M=D")

        return assembly_code

    def _pop_static(self, i: int, memory_segment: str, filename: str) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")
        assembly_code.append(f"@{filename}.{i}")
        assembly_code.append("M=D")

        return assembly_code

    def _push_static(self, i: int, memory_segment: str, filename: str) -> List[str]:
        assembly_code = []

        assembly_code.append(f"@{filename}.{i}")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")
        assembly_code.append("A=M-1")
        assembly_code.append("M=D")

        return assembly_code

    def _pop_temp(self, i: int, memory_segment: str, filename: str) -> List[str]:
        assembly_code = []

        mem = ram['temp'] + i
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")
        assembly_code.append(f"@{mem}")
        assembly_code.append("M=D")

        return assembly_code

    def _push_temp(self, i: int, memory_segment: str, filename: str) -> List[str]:
        assembly_code = []

        mem = ram['temp'] + i
        assembly_code.append(f"@{mem}")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")
        assembly_code.append("A=M-1")
        assembly_code.append("M=D")

        return assembly_code

    def _pop_pointer(self, i: int, memory_segment: str, filename: str) -> List[str]:
        mem = ["this", "that"]

        assembly_code = []

        mem = ram[mem[i]]
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")
        assembly_code.append(f"@{mem}")
        assembly_code.append("M=D")

        return assembly_code

    def _push_pointer(self, i: int, memory_segment: str, filename: str) -> List[str]:
        mem = ["this", "that"]

        assembly_code = []

        mem = ram[mem[i]]
        assembly_code.append(f"@{mem}")
        assembly_code.append("D=M")
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("M=M+1")
        assembly_code.append("A=M-1")
        assembly_code.append("M=D")

        return assembly_code


class Branching:
    operations = {
        'label': '_%s_to_assembly',
        'if-goto': '_%s_to_assembly',
        'goto': '_%s_to_assembly',
    }

    def to_assembly(self, operation: str, label_name: str) -> List[str]:
        try:
            op = self.operations[operation] % operation.replace('-', '_')
        except KeyError as e:
            print(f'Provided {operation=} is not supported by compiler instruction.')
            raise e from None
        else:
            return getattr(self, op)(label_name)

    def _label_to_assembly(self, label_name: str) -> List[str]:
        assembly_code = []
        assembly_code.append(f"({label_name})")
        return assembly_code

    def _if_goto_to_assembly(self, label_name: str) -> List[str]:
        assembly_code = []
        assembly_code.append(f"@{ram['sp']}")
        assembly_code.append("AM=M-1")
        assembly_code.append("D=M")
        assembly_code.append(f"@{label_name}")
        assembly_code.append(f"D;JNE")

        return assembly_code

    def _goto_to_assembly(self, label_name: str) -> List[str]:
        assembly_code = []
        assembly_code.append(f"@{label_name}")
        assembly_code.append(f"0;JMP")

        return assembly_code

functions = Functions()

def to_assembly(instructions: List[str], filename: str) -> List[str]:
    assembly_code = []
    commands = Commands()
    stack_man = StackManipulation()
    branching = Branching()

    for instr in instructions:
        instr = instr.strip()
        if instr.startswith("//") or instr == '':
            continue

        if '//' in instr:
            comment = instr.find('//')
            instr = instr[:comment].strip()

        print(f"starting to compile the instruction: {instr}")
        assembly_code.append("// "+instr)


        ingr = instr.split(" ")
        if len(ingr) == 3:
            if ingr[0] in ['function', 'call']:
                code = functions.to_assembly(ingr[0], ingr[1], int(ingr[2]))
            else:
                code = stack_man.to_assembly(int(ingr[2]), ingr[1], filename, ingr[0])
        elif len(ingr) == 2:
            code = branching.to_assembly(ingr[0], ingr[1])
        elif len(ingr) == 1:
            code = commands.to_assembly(ingr[0])
        else:
            raise ValueError(f"Provided instruction is not correct: {instr}!")
        assembly_code += code
        assembly_code.append("")

    return assembly_code

def main(filename: str) -> None:
    if os.path.isfile(filename):
        print(f"Starting to compile file: {filename}")
        assembly_lines = handle_file(filename)

        output_file = filename.replace('vm', 'asm')
        with open(output_file, 'w') as f:
            for line in assembly_lines:
                f.write(f"{line}\n")

        print('Done')

    assembly_lines = []
    if 'Sys.vm' in os.listdir(filename):

        assembly_lines.append("//init stack pointer")
        assembly_lines += functions.to_assembly('sys_init', 'Sys.init', 0)
    assembly_lines.append("")

    for file in os.listdir(filename):
        if file.endswith(".vm"):
            print(f"Starting to compile file: {filename+file}")
            assembly_lines.append(f"//file {file}")
            assembly_lines.append("")
            assembly_lines += handle_file(filename + file)

    output_file = filename.split("/")[-2] + ".asm"
    with open(filename+output_file, 'w') as f:
        for line in assembly_lines:
            f.write(f"{line}\n")

    print('Done')

def handle_file(filename: str) -> List[str]:
    source_code = open(filename, 'r')
    lines = source_code.readlines()
    assembly_lines = to_assembly(lines, filename.split("/")[-1].split(".")[0])
    source_code.close()

    return assembly_lines



if __name__ == '__main__':
    main(sys.argv[1])