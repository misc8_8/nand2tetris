from __future__ import annotations

import copy
import re
from typing import List, Tuple
import os


class TokenTypeValidator:
    keywords = [
        'class', 'constructor', 'function',
        'method', 'field', 'static', 'var', 'int',
        'char', 'boolean', 'void', 'true', 'false', 'null',
        'this', 'that', 'let', 'do', 'if', 'else', 'while', 'return'
    ]

    symbols = [
        '{', '}', '(', ')', '[', ']', '.', ',', ';', '+', '-', '*', '/', '&', '|', '<', '>', '=', '~'
    ]

    def is_argument(self, token) -> bool:
        args = token.split(" ")
        if len(args) < 2:
            return False
        for arg in args:
            if '"' in arg:
                return False
        return True

    def _is_method(self, token) -> bool:
        if '(' in token and ')' in token:
            return self._is_identifier(token.split('(')[0])
        return False

    def _is_array(self, token) -> bool:
        return '[' in token and ']' in token

    def _is_identifier(self, token: str) -> bool:
        if '_' in token:
            token = token.replace("_", "")
        if not token.isalnum():
            return False

        return not self._is_int(token[0])

    def _is_string(self, token: str) -> bool:
        return token.startswith('"') or token.startswith("'")

    def _is_keyword(self, token: str) -> bool:
        return token in self.keywords

    def _is_symbol(self, token: str) -> bool:
        return token in self.symbols

    def _is_int(self, token) -> bool:
        try:
            int(token)
        except ValueError:
            return False
        else:
            return True


class Tokenizer:
    validator = TokenTypeValidator()

    def define_types(self, token: str) -> List[Tuple[str, str]]:
        types = []

        print(f"Defining type for {token=}")

        if len(token) > 2:
            if '.' in token:
                types += self._dissect_object_type(token)
                return types
            elif self.validator._is_method(token):
                types += self._define_method_type(token)
                return types
            elif self.validator._is_array(token):
                types += self._define_array_type(token)
                return types
            elif self.validator.is_argument(token):
                types += self._dissect_argument_type(token)
                return types

        first_els, token = self._dissect_first_el(token)
        last_els, token = self._dissect_last_el(token)

        if len(token) == 0:
            return first_els + last_els

        if self.validator._is_symbol(token):
            types.append((token, 'symbol'))
        elif self.validator._is_int(token):
            types.append((token, 'integerConstant'))
        elif self.validator._is_string(token):
            types.append((token.replace('"', ""), 'stringConstant'))
        elif self.validator._is_keyword(token):
            types.append((token, 'keyword'))
        elif self.validator._is_identifier(token):
            types.append((token, 'identifier'))
        else:
            raise ValueError(f"Provided value can not be distinguished: {token=}!")

        types = first_els + types + last_els
        return types

    def _dissect_first_el(self, token: str) -> Tuple[List[Tuple[str, str]], str]:
        first_els = []
        for i in range(len(token)):
            try:
                first_token_el = token[i]
            except IndexError:
                break
            else:
                if not self.validator._is_symbol(first_token_el):
                    break
                first_els.append((first_token_el, 'symbol'))
        token = token[len(first_els):]
        return first_els, token

    def _dissect_last_el(self, token: str) -> Tuple[List[Tuple[str, str]], str]:
        last_els = []
        for i in range(len(token) - 1, 0, -1):
            try:
                last_token_el = token[i]
            except IndexError:
                break
            else:
                if not self.validator._is_symbol(last_token_el):
                    break
                last_els.insert(0, (last_token_el, 'symbol'))
                token = token[:-1]
        return last_els, token

    def _dissect_object_type(self, token: str) -> List[Tuple[str, str]]:
        types = []

        args = token.split('.')
        args.insert(1, '.')
        for arg in args:
            if arg == '':
                continue
            types += self.define_types(arg)

        return types

    def _dissect_argument_type(self, token: str) -> List[Tuple[str, str]]:
        types = []

        args = token.split(' ')
        for arg in args:
            if arg == '':
                continue
            types += self.define_types(arg)

        return types

    def _define_method_type(self, token: str) -> List[Tuple[str, str]]:
        types = []

        args = token.split('(', 1)
        args.insert(1, "(")
        func_args = args[2].split(", ")
        len_func_args = len(func_args)
        inserted = 1
        for i in range(len_func_args):
            if i + 1 >= len_func_args:
                break
            func_args.insert(i + 1 * inserted, ',')
            inserted += 1

        args = args[:2] + func_args[:]
        for arg in args:
            if arg == '':
                continue
            types += self.define_types(arg)

        return types


    def _define_array_type(self, token: str) -> List[Tuple[str, str]]:
        types = []
        args = []
        current_arg = ''
        for char in token:
            if char == '[':
                args.append(current_arg)
                current_arg = ''
                args.append('[')
                continue
            current_arg += char
        args.append(current_arg)

        for arg in args:
            if arg == '' or arg == ' ':
                continue
            types += self.define_types(arg)

        return types

class SymbolTable:
    CLASS = 'class'
    SUB = 'subroutine'
    def __init__(self):
        self.class_level = {}
        self.subroutine_level = {}
        self._type_counts = {
            'static': 0,
            'argument': 0,
            'field': 0,
            'local': 0
        }
        self._class_lvl_count = 0
        self._subroutine_lvl_count = 0

    def init_new(self, level:str):
        setattr(self, f"{level}_level", {})

    def add(self, level:str, value: dict):
        table = getattr(self, f"{level}_level")
        lvl_count = getattr(self, f"_{level}_lvl_count")

        name = value["name"]
        del value['name']

        value["#"] = lvl_count

        self._type_counts[value["kind"]] += 1
        table[name] = value

        setattr(self, f"{level}_level", table)
        setattr(self, f"_{level}_lvl_count", lvl_count+1)

    def retrieve(self, level: str, name: str):
        table = getattr(self, f"{level}_level")
        try:
            values = table[name]
        except KeyError:
            pass
        else:
            return values

        if level == SymbolTable.SUB:
            table = getattr(self, "class_level")
            try:
                values = table[name]
            except KeyError:
                pass
            else:
                return values

        raise ValueError(f"Provided {name=} is not supported by any variable starting from {level=}!")

    def count(self, type: str):
        return self._type_counts[type]

class VMWriter:
    def __init__(self, tokens, symbol_table: SymbolTable):
        self.symbol_table = symbol_table
        self.tokens = tokens
        self.vm_code = []

        self.if_count = 0
        self.while_count = 0
        self.class_name = ''
        self.convert_kind = {
            'argument': 'ARG',
            'static': 'STATIC',
            'local': 'local',
            'field': 'THIS'
        }


    def compile(self) -> List[str]:
        cursor = 0
        for _ in range(len(self.tokens)):
            try:
                text, type = self.tokens[cursor]
            except IndexError:
                self.vm_code.append('return')
                break

            if text in ['static', 'field']:
                cursor += self._parse_class_var(cursor)
            elif text in ["constructor", "function", "method"]:
                cursor += self._parse_class_subroutine(cursor)
            elif cursor == 1:
                self.class_name = text
                cursor += 1
            else:
                cursor += 1
        return self.vm_code

    def _parse_class_var(self, i) -> int:
        tokens = self.tokens[i:]

        vars = []
        value = {}
        next = 'kind'
        state = {
            "kind": "type",
            "type": "name",
            "name": "append",
            "append": "name"
        }

        for j in range(len(tokens)):
            text, type = tokens[j]
            if next == 'append':
                vars.append(copy.deepcopy(value))
                next = "name"
            if text == ';':
                break
            if text == ',':
                continue

            value[next] = text
            next = state[next]


        self.symbol_table.init_new(SymbolTable.CLASS)
        for var in vars:
            self.symbol_table.add(SymbolTable.CLASS, var)
        return j + 1

    def _parse_class_subroutine(self, i) -> int:
        tokens = self.tokens[i:]
        self.symbol_table.init_new(SymbolTable.SUB)
        sub_type = tokens[0][0]

        cursor = 0
        cursor += 2
        text, type = tokens[cursor]
        cursor += 1
        func_name = "{}.{}".format(self.class_name, text)
        cursor += 1 #(
        cursor += self._parse_parameter_list(i + cursor, sub_type == 'method')
        cursor += 1 # )
        cursor += 1 # {

        for _ in range(len(tokens[cursor:])):
            text, type = tokens[cursor]
            if text == 'var':
                cursor += self._parse_var_dec(i + cursor)
                continue
            else:
                break

        n_args = self.symbol_table.count('local')
        self.vm_code.append(f"function {func_name} {n_args}")

        if sub_type == 'constructor':
            n_fields = self.symbol_table.count('field')
            self.vm_code.append(f'push constant {n_fields}')
            self.vm_code.append('call Memory.alloc 1')
            self.vm_code.append('pop POINTER 0')
        elif sub_type == 'method':
            self.vm_code.append('push ARG 0')
            self.vm_code.append('pop POINTER 0')

        cursor += self._parse_statements(i + cursor)
        cursor += 1

        return cursor

    def _parse_parameter_list(self, i, is_method: bool) -> int:
        tokens = self.tokens[i:]
        cursor = 0

        vars = []
        value = {"kind": "argument"}
        next = 'type'
        state = {
            "type": "name",
            "name": "append",
            "append": "type"
        }
        number = 0


        for _ in range(len(tokens)):
            text, type = tokens[cursor]
            if is_method:
                value['name'] = 'this'
                value['type'] = text
                is_method = False
                next = "append"

            if next == 'append':
                value['#'] = number
                vars.append(copy.deepcopy(value))
                number+=1
                next = state[next]

            if text == ')':
                break
            if text == ',':
                continue


            value[next] = text
            next = state[next]

            cursor += 1

        for var in vars:
            self.symbol_table.add(SymbolTable.SUB, var)

        return cursor

    def _parse_sub_body(self, i) -> int:
        tokens = self.tokens[i:]
        cursor = 0
        for _ in range(len(tokens)):
            text, type = tokens[cursor]
            if text == '}':
                cursor += 1
                break
            elif text == 'var':
                cursor += self._parse_var_dec(i + cursor)
                continue
            elif text in ['let', 'if', 'while', 'do', 'return']:
                cursor += self._parse_statements(i + cursor)
            else:
                cursor += 1

        return cursor

    def _parse_var_dec(self, i) -> int:
        tokens = self.tokens[i:]

        vars = []
        value = {"kind": "local"}
        next = 'type'
        state = {
            "type": "skip",
            "skip": "name",
            "name": "append",
            "append": "name"
        }


        for j in range(len(tokens)):
            text, type = tokens[j]
            if next == 'append':
                vars.append(copy.deepcopy(value))
                next = state[next]
            if next == 'skip':
                next = state[next]
                continue

            if text == ';':
                break
            if text == ',':
                continue

            value[next] = text
            next = state[next]

        for var in vars:
            self.symbol_table.add(SymbolTable.SUB, var)

        return j + 1

    def _parse_statements(self, i) -> int:
        tokens = self.tokens[i:]
        cursor = 0
        for _ in tokens:
            text, type = tokens[cursor]
            if text in ['let', 'if', 'while', 'do', 'return']:
                cursor += self._parse_statement(i + cursor)
            else:
                break

        return cursor

    def _parse_statement(self, i) -> int:
        text, type = self.tokens[i]
        method = "_parse_%s_stat" % text
        return getattr(self, method)(i)

    def _parse_let_stat(self, i) -> int:
        tokens = self.tokens[i+1:]
        cursor = 0

        text, type = tokens[cursor]
        values = self.symbol_table.retrieve(SymbolTable.SUB, text)
        kind = self.convert_kind[values["kind"]]

        cursor += 1
        text, type = tokens[cursor]

        if text == '[':  # array assignment
            cursor += 2
            cursor += self._parse_expression(i + cursor)
            cursor += 1

            self.vm_code.append(f"push {kind} {values['#']}")
            self.vm_code.append("add")
            self.vm_code.append("pop temp 0")

            cursor += 1
            cursor += self._parse_expression(i + cursor)


            self.vm_code.append('push temp 0')
            self.vm_code.append('pop pointer 1')
            self.vm_code.append('pop that 0')
        else:
            cursor += 2
            cursor += self._parse_expression(i + cursor)

            self.vm_code.append(f"pop {kind} {values['#']}")

        return cursor

    def _parse_if_stat(self, i) -> int:
        tokens = self.tokens[i:]
        cursor = 0

        next_is_expression = False
        next_is_statements = False

        l1 = "IF_TRUE{}".format(self.if_count)
        l2 = "IF_FALSE{}".format(self.if_count)
        l3 = "IF_END{}".format(self.if_count)

        for _ in range(len(tokens)):
            if next_is_expression:
                cursor += self._parse_expression(i + cursor)
                next_is_expression = False

                self.vm_code.append(f"if-goto {l1}")
                self.vm_code.append(f"goto {l2}")
                self.vm_code.append(f"label {l1}")
                self.if_count += 1

                continue
            elif next_is_statements:
                cursor += self._parse_statements(i + cursor)

                self.vm_code.append(f"goto {l3}")
                next_is_statements = False
                continue

            text, type = tokens[cursor]
            cursor += 1

            if text == '(':
                next_is_expression = True
            elif text == '{':
                next_is_statements = True
            elif text == '}':
                self.vm_code.append(f"label {l2}")
                text, type = tokens[cursor]
                if text == 'else':
                    cursor += self._parse_if_stat(i+cursor)
                else:
                    break
        self.vm_code.append(f"label {l3}")
        return cursor

    def _parse_while_stat(self,  i) -> int:
        tokens = self.tokens[i:]
        cursor = 0

        l1 = "WHILE_EXP{}".format(self.while_count)
        l2 = "WHILE_END{}".format(self.while_count)
        self.while_count += 1

        self.vm_code.append(f"label {l1}")

        next_is_expression = False
        next_is_statements = False
        for _ in range(len(tokens)):
            if next_is_expression:
                cursor += self._parse_expression(i + cursor)
                self.vm_code.append(f"not")

                next_is_expression = False
                continue
            elif next_is_statements:
                self.vm_code.append(f"if-goto {l2}")
                cursor += self._parse_statements(i + cursor)
                self.vm_code.append(f"goto {l1}")
                self.vm_code.append(f"label {l2}")

                next_is_statements = False
                continue

            text, type = tokens[cursor]
            cursor += 1

            if text == '(':
                next_is_expression = True
            elif text == '{':
                next_is_statements = True
            elif text == '}':
                break

        return cursor

    def _parse_do_stat(self, i) -> int:
        cursor = 0
        cursor += 1 #do
        var_name, type = self.tokens[cursor+i]
        cursor += 1
        cursor += self._parse_subroutine_call(i + cursor, var_name)

        self.vm_code.append("pop temp 0")

        return cursor

    def _parse_return_stat(self, i) -> int:
        cursor = 0
        next_expression = False
        tokens = self.tokens[i:]

        for _ in tokens:
            text, type = tokens[cursor]
            if text == ';':
                cursor += 1
                self.vm_code.append('push constant 0') # void method
                break
            if next_expression:
                cursor += self._parse_expression(i + cursor)
                cursor += 1
                next_expression = False
                break
            next_expression = True
            cursor += 1

        return cursor

    def _parse_expression(self, i) -> int:
        available_ops = ('+', '-', '*', '/', '&', '|', '<', '>', '=')
        op_table = {
            '+': 'ADD', '-': 'SUB', '&': 'AND', '|': 'OR',
            '<': 'LT', '>': 'GT', '=': 'EQ'
        }
        tokens = self.tokens[i:]
        cursor = 0

        cursor += self._parse_term(i + cursor)
        for _ in range(len(tokens)):
            text, type = tokens[cursor]
            op = text
            cursor += 1
            #
            if op == ';':
                break

            if op not in available_ops:
                cursor -= 1
                break

            try:
                cursor += self._parse_term(i + cursor)
            except ValueError:
                pass

            if op in op_table:
                self.vm_code.append(f"{op_table[op].lower()}")
            elif op == '*':
                self.vm_code.append('call Math.multiply 2')
            elif op == '/':
                self.vm_code.append('call Math.divide 2')

        return cursor

    def _parse_term(self, i) -> int:
        cursor = 0
        text, type = self.tokens[i]
        if type == "stringConstant":
            self._compile_string(text)
            cursor += 1
            return cursor
        elif type == 'integerConstant':
            self.vm_code.append(f"push constant {int(text)}")
            cursor += 1
            return cursor
        elif text in ('true', 'false', 'null'):
            int_mirror = {
                "true": 1,
                "false": 0,
                "null": 0
            }
            int_value = int_mirror[text]
            self.vm_code.append(f"push constant {int_value}")
            cursor += 1
            return cursor
        elif text == 'this':
            self.vm_code.append(f"push pointer 0")
            cursor += 1
            return cursor
        elif text in ('-', '~'):
            cursor += 1
            cursor += self._parse_term(cursor + i)
            if text == '-':
                self.vm_code.append(f"neg")
            else:
                self.vm_code.append(f"not")
            return cursor
        elif type == 'identifier':
            var_name = text
            cursor += 1
            text, type = self.tokens[i + cursor]
            if text == '[':
                cursor += 1
                cursor += self._parse_expression(i + cursor)
                cursor += 1

                values = self.symbol_table.retrieve(SymbolTable.SUB, var_name)
                kind = self.convert_kind[values['kind']]
                self.vm_code.append(f'push {kind} {values["#"]}')
                self.vm_code.append(f'add')
                self.vm_code.append(f'pop pointer 1')
                self.vm_code.append(f'push that 0')
            elif text == '(' or text == '.':
                cursor += self._parse_subroutine_call(i + cursor, var_name)
            else:
                values = self.symbol_table.retrieve(SymbolTable.SUB, var_name)
                kind = self.convert_kind[values['kind']]
                self.vm_code.append(f'push {kind} {values["#"]}')
            return cursor
        elif text == '(':
            cursor += self._parse_expression(i + 1)
            cursor += 2
            return cursor
        else:
            raise ValueError(f"Provided token {text=} an {type=} are not supported")

    def _compile_string(self, text):
        self.vm_code.append(f"push constant {len(text)}")
        self.vm_code.append('call String.new 1')

        for char in text:
            self.vm_code.append(f"push constant {ord(char)}")
            self.vm_code.append('call String.appendChar 2')

    def _parse_subroutine_call(self, i, var_name) -> int:
        tokens = self.tokens[i:]
        cursor = 0
        n_args = 0

        text, type = tokens[cursor]
        if text == '.':
            cursor += 1
            sub_name, type = tokens[cursor]

            try:
                values = self.symbol_table.retrieve(SymbolTable.SUB, var_name)

            except ValueError:
                func_name = "{}.{}".format(var_name, sub_name)
            else:
                kind = self.convert_kind[values['kind']]
                self.vm_code.append(f"push {kind} {values['#']}")
                func_name = "{}.{}".format(values['type'], sub_name)
                n_args += 1

        elif text == '(':
            sub_name = var_name
            func_name = "{}.{}".format(self.class_name, sub_name)
            n_args += 1
            self.vm_code.append(f"push pointer 0")
        else:
            raise ValueError(f"Provided token {text=} an {type=} are not supported")

        cursor += 1
        cursor_app, n_args_app = self._parse_expression_list(cursor + i)
        cursor += cursor_app + 1
        n_args += n_args_app
        self.vm_code.append(f'call {func_name} {n_args}')

        return cursor

    def _parse_expression_list(self, i) -> Tuple[int, int]:
        tokens = self.tokens[i:]
        cursor = 0
        n_args = 0

        for _ in range(len(tokens)):
            text, type = tokens[cursor]
            if text == ')':
                cursor += 1
                break
            if text == ',' or text == '(':
                cursor += 1
                continue
            try:
                cursor += self._parse_expression(i + cursor)
                n_args += 1
            except ValueError:
                n_args += 1
                cursor += 1
                break

        return cursor, n_args


def compile_file(tokens: List[Tuple[str, str]]):
    symbol_table = SymbolTable()
    compiler = VMWriter(tokens, symbol_table)
    vm_code = compiler.compile()
    return vm_code

def tokenize_file(lines: List[str]) -> List[Tuple[str, str]]:
    tokens = Tokenizer()

    tokens_types = []
    comments = False
    for line in lines:
        line = line.strip()
        if '*/' in line:
            comments = False
            continue
        if ("/**" in line and '*/' in line) or line == '' or line.startswith('//') or comments:
            continue
        if '/**' in line:
            comments = True
            continue
        if '//' in line:
            comment = line.find('//')
            line = line[:comment].strip()

        print(f"Compiling line: {line}")
        for token in _extract_tokens(line):
            tokens_types += tokens.define_types(token)

    return tokens_types


def _extract_tokens(line: str) -> List[str]:
    tokens = []
    while len(line) > 0:
        new_token = _extract_token(line)
        tokens.append(new_token)
        new_start = line.find(new_token) + len(new_token) + 1
        line = line[new_start:]
    return tokens


def _extract_token(line: str) -> str:
    token = ''
    open_quotation = False
    open_bracket = 0
    open_square_bracket = 0
    for el in line:
        if el in ["'", '"']:
            open_quotation = not open_quotation
        if el == '(':
            open_bracket += 1
        if el == ')':
            open_bracket -= 1
        if el == '[':
            open_square_bracket += 1
        if el == ']':
            open_square_bracket -= 1

        if el == ' ':
            if open_quotation or open_bracket > 0 or open_square_bracket > 0:
                token += el
                continue
            if len(token) == 0:
                continue
            break

        token += el

    return token


def main(filename: str) -> None:
    if os.path.isfile(filename):
        print(f"Starting to compile file: {filename}")
        vm_code = handle_file(filename)

        output_file = filename.replace('jack', 'vm')
        with open("opt/" + output_file, 'w') as f:
            for line in vm_code:
                f.write(f"{line}\n")

        print('Done')
        return

    for file in os.listdir(filename):
        if file.endswith(".jack"):
            print(f"Starting to compile file: {filename + file}")
            vm_code = handle_file(filename + file)
            output_file = file.replace('jack', 'vm')
            with open(filename + "opt/" + output_file, 'w') as f:
                for line in vm_code:
                    f.write(f"{line}\n")

    print('Done')


def handle_file(filename: str) -> List[str]:
    source_code = open(filename, 'r')
    lines = source_code.readlines()
    tokens = tokenize_file(lines)
    vm_code = compile_file(tokens)
    source_code.close()

    return vm_code


if __name__ == '__main__':
    # main(sys.argv[1])
    main("./Pong/")
