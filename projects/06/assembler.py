import copy
import sys


symbol_table = {
            "R0": 0,
            "R1": 1,
            "R2": 2,
            "R3": 3,
            "R4": 4,
            "R5": 5,
            "R6": 6,
            "R7": 7,
            "R8": 8,
            "R9": 9,
            "R10": 10,
            "R11": 11,
            "R12": 12,
            "R13": 13,
            "R14": 14,
            "R15": 15,
            "SCREEN": 16384,
            "KBD": 24576,
            "SP": 0,
            "LCL": 1,
            "ARG": 2,
            "THIS": 3,
            "THAT": 4,
        }

comp = {
    '0': '0101010',
    '1': '0111111',
    '-1': '0111010',
    'D': '0001100',
    'A': '0110000',
    '!D': '0001101',
    '!A': '0110001',
    '-D': '0001111',
    '-A': '0110011',
    'D+1': '0011111',
    'A+1': '0110111',
    'D-1': '0001110',
    'A-1': '0110010',
    'D+A': '0000010',
    'D-A': '0010011',
    'A-D': '0000111',
    'D&A': '0000000',
    'D|A': '0010101',
    'M': '1110000',
    '!M': '1110001',
    '-M': '1110011',
    'M+1': '1110111',
    'M-1': '1110010',
    'D+M': '1000010',
    'D-M': '1010011',
    'M-D': '1000111',
    'D&M': '1000000',
    'D|M': '1010101',
}

dest = {
    'null': '000',
    'M': '001',
    'D': '010',
    'MD': '011',
    'A': '100',
    'AM': '101',
    'AD': '110',
    'AMD': '111'
}

jump = {
    'null': '000',
    'JGT': '001',
    'JEQ': '010',
    'JGE': '011',
    'JLT': '100',
    'JNE': '101',
    'JLE': '110',
    'JMP': '111'
}

n = 16


def parser(filename: str) -> list:
    source_code = open(filename, 'r')
    lines = source_code.readlines()
    lines = first_pass(lines)
    binary = translate_to_binary(lines)
    source_code.close()
    return binary


def first_pass(source_code: list) -> list:
    pc = -1
    clean_source_code = []
    for line in source_code:
        line = line.strip()
        if line.startswith('//') or line == '':
            continue

        if not line.startswith('('):
            clean_source_code.append(line)
            pc += 1
            continue
        symbol_key = line.replace('(', '').replace(')', '')
        symbol_table[symbol_key] = pc + 1
    return clean_source_code

def translate_to_binary(source_code: list) -> list:
    binary_source_code = []
    for line in source_code:
        binary_line = decode_instruction(line)
        binary_source_code.append(binary_line)
    return binary_source_code

def decode_instruction(line: str) -> str:
    if '//' in line:
        comment = line.find('//')
        line = line[:comment]

    line = line.strip()
    if line[0] == '@':
        line = line.replace('@', '')
        try:
            memory = '{0:015b}'.format(int(line))
        except ValueError:
            pass
        else:
            return '0' + memory

        try:
            line = symbol_table[line]
        except KeyError:
            global n
            m = copy.deepcopy(n)
            symbol_table[line] = m
            line = m
            n += 1
        finally:
            memory = '{0:015b}'.format(int(line))
            return '0' + memory

    instructions = line.split(';')
    dest_comp = instructions[0].split('=')
    if len(dest_comp) == 2:
        dest_instruction = dest_comp[0]
        comp_instruction = dest_comp[1]
    else:
        dest_instruction = 'null'
        comp_instruction = dest_comp[0]

    try:
        instr_dest = dest[dest_instruction]
    except KeyError as e:
        print(f"DST instruction failed: {e}")
        return ''

    try:
        instr_comp = comp[comp_instruction]
    except KeyError as e:
        print(f"COMP instruction failed: {e}")
        return ''

    try:
        instr_jump = jump[instructions[1]]
    except KeyError as e:
        print(f"JMP instruction failed: {e}")
        return ''
    except IndexError:
        instr_jump = jump['null']

    return '111' + instr_comp + instr_dest + instr_jump


def main(filename: str):
    binary_source_code = parser(filename)
    output_file = filename.replace('asm', 'hack')

    with open(output_file, 'w') as f:
        for line in binary_source_code:
            f.write(f"{line}\n")

    print('Done')

if __name__ == '__main__':
    main(sys.argv[1])